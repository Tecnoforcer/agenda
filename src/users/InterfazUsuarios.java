package users;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import agendaDinamica.Contacto;
import login.InterfazLogin;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class InterfazUsuarios extends JFrame {

	private JPanel contentPane;
	private JLabel lbl_titulo;
	private JLabel lbl_nombre;
	private JTextField textField_nombre;
	private JLabel lbl_Contrese�a;
	private JButton btn_saveUSer;
	private JButton btn_Login;
	private JPasswordField passwordField_contrase�a;
	private JLabel lbl_ConfirmContrase�a;
	private JPasswordField passwordField_confirmar_Contrase�a;
	private ArrayList<Usuario> users;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					InterfazUsuarios frame = new InterfazUsuarios();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarios() {
		users=new ArrayList<Usuario>();
		users=IOdatUsers.cargarDatos();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 459, 481);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lbl_titulo = new JLabel("NEW USER");
		lbl_titulo.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 25));
		lbl_titulo.setBounds(132, 30, 209, 55);
		contentPane.add(lbl_titulo);

		lbl_nombre = new JLabel("Nombre");
		lbl_nombre.setBounds(117, 123, 46, 14);
		contentPane.add(lbl_nombre);

		textField_nombre = new JTextField();
		textField_nombre.setBounds(255, 123, 86, 20);
		contentPane.add(textField_nombre);
		textField_nombre.setColumns(10);

		lbl_Contrese�a = new JLabel("Contrase\u00F1a");
		lbl_Contrese�a.setBounds(117, 154, 86, 14);
		contentPane.add(lbl_Contrese�a);

		btn_saveUSer = new JButton("ADD USER");
		btn_saveUSer.addMouseListener(new Btn_saveUSerMouseListener());
		btn_saveUSer.setBounds(171, 262, 89, 23);
		contentPane.add(btn_saveUSer);

		btn_Login = new JButton("Log In");
		btn_Login.addMouseListener(new Btn_NewButton_1MouseListener());
		btn_Login.setBounds(344, 408, 89, 23);
		contentPane.add(btn_Login);

		passwordField_contrase�a = new JPasswordField();
		passwordField_contrase�a.setBounds(255, 154, 86, 20);
		contentPane.add(passwordField_contrase�a);

		lbl_ConfirmContrase�a = new JLabel("Confirmar Contrase\u00F1a");
		lbl_ConfirmContrase�a.setBounds(117, 179, 128, 14);
		contentPane.add(lbl_ConfirmContrase�a);

		passwordField_confirmar_Contrase�a = new JPasswordField();
		passwordField_confirmar_Contrase�a.setBounds(255, 179, 86, 20);
		contentPane.add(passwordField_confirmar_Contrase�a);
	}

	private class Btn_saveUSerMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			String nombre = "";
			String passwoerd = "";
			String checkPasswoerd = "";
			boolean correct=false;
			boolean valido=false;
			nombre = textField_nombre.getText();
			char[] pass = passwordField_contrase�a.getPassword();

			for (int i = 0; i < pass.length; i++) {
				passwoerd += pass[i];
			}
			char[] checkPass = passwordField_confirmar_Contrase�a.getPassword();

			for (int i = 0; i < pass.length; i++) {
				checkPasswoerd += checkPass[i];
			}
			
			if ((nombre != "" && nombre != null)&&(passwoerd != "" && passwoerd != null)&&(checkPasswoerd != "" && checkPasswoerd != null)) {
				valido=true;
			}
			
			if (valido&& (passwoerd.equals(checkPasswoerd))) {
				Usuario usuarioNuevo=new Usuario(nombre, passwoerd);
				users.add(usuarioNuevo);
				
				IOdatUsers.guardar_Datos(users);
				JOptionPane.showMessageDialog(null, "se ha guardado el usuario", "guardado", 1);
				correct=true;
			}else {
				correct=false;
			}
			
			if (!correct) {
				JOptionPane.showMessageDialog(null, "no se ha podido guardar el usuario", "ERROR", 0);
				
			}else {
				InterfazLogin il =new InterfazLogin();
				il.setVisible(true);
				dispose();
			}

		}
	}

	private class Btn_NewButton_1MouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			InterfazLogin il =new InterfazLogin();
			il.setVisible(true);
			dispose();
		}
	}
}
