package users;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import agendaDinamica.Contacto;

public class IOdatUsers {
	private final static String directorio = "datosUsuario/";
	private final static String fichero = "datosUsuario/usuarios.dat";

	public static void guardar_Datos(ArrayList<Usuario> vUsuarios) {
		File dir = new File(directorio);
		File file = new File(fichero);
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		if (!dir.exists()) {
			dir.mkdir();
		}

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// e.printStackTrace();
			}
		} else {
			try {
				fos = new FileOutputStream(file);
				oos = new ObjectOutputStream(fos);
				System.out.println(vUsuarios.size());
				oos.writeObject(vUsuarios);
				
			} catch (FileNotFoundException e) {
				// e.printStackTrace();
			} catch (IOException e) {
				// e.printStackTrace();
			} finally {
				try {
					oos.close();
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static ArrayList<Usuario> cargarDatos() {
		ArrayList<Usuario> vUsuarios = new ArrayList<Usuario>();
		File dir = new File(directorio);
		File file = new File(fichero);
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		if (!dir.exists()) {
			dir.mkdir();
		}

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return vUsuarios;
		}
		try {
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);

			vUsuarios = (ArrayList<Usuario>) ois.readObject();
			
		} catch (FileNotFoundException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// e.printStackTrace();
		} finally {
			try {
				if (ois!=null)
					ois.close();
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		}

		return vUsuarios;

	}
}
