package users;

import java.io.Serializable;

public class Usuario implements Serializable{
	private String nombre;
	private String passWord;

	public Usuario(String nombre, String passWord) {
		this.nombre = nombre;
		this.passWord = passWord;
	}

	public String getNombre() {
		return nombre;
	}

	public String getPassWord() {
		return passWord;
	}
// para mas adelante
	
//	public void setNombre(String nombre) {
//		this.nombre = nombre;
//	}

//	public void setPassWord(String passWord) {
//		this.passWord = passWord;
//	}

}
