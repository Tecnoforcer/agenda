package login;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.TextUI;

import agendaDinamica.Interfaz;
import users.IOdatUsers;
import users.InterfazUsuarios;
import users.Usuario;

import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class InterfazLogin extends JFrame {

	private JPanel contentPane;
	private JButton btn_ENTRAR;
	private JPasswordField passwordField;
	private JTextField txt_Usuario;
	private JLabel lblimagen;
	private final String usuario = "asdf";
	private final String contraseņa = "asdf";
	private JButton btn_aņadirUsuario;
	private ArrayList<Usuario> users;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazLogin frame = new InterfazLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazLogin() {
		users=new ArrayList<Usuario>();
		users=IOdatUsers.cargarDatos();
		System.out.println(users.size());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 567);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btn_ENTRAR = new JButton("ENTRAR");
		btn_ENTRAR.setEnabled(false);
		btn_ENTRAR.setBounds(79, 416, 274, 23);
		contentPane.add(btn_ENTRAR);

		passwordField = new JPasswordField();
		passwordField.addKeyListener(new PasswordFieldKeyListener());
		passwordField.setFont(new Font("Tahoma", Font.ITALIC, 11));
		passwordField.setForeground(Color.LIGHT_GRAY);

		passwordField.setEchoChar((char) 0);
		passwordField.setText("contraseņa");
		passwordField.setBounds(79, 360, 274, 20);
		contentPane.add(passwordField);

		txt_Usuario = new JTextField();
		txt_Usuario.addKeyListener(new Txt_UsuarioKeyListener());
		txt_Usuario.setFont(new Font("Tahoma", Font.ITALIC, 11));
		txt_Usuario.setForeground(Color.LIGHT_GRAY);
		txt_Usuario.setText("usuario");
		txt_Usuario.setBounds(79, 302, 274, 20);
		contentPane.add(txt_Usuario);
		txt_Usuario.setColumns(10);

		lblimagen = new JLabel("");
		lblimagen.setFocusable(true);
		lblimagen.requestFocus();
		lblimagen.setIcon(new ImageIcon("C:\\Users\\alumno\\eclipse-workspace\\LOGIN\\450_1000.jpg"));
		lblimagen.setBounds(10, 61, 414, 210);
		contentPane.add(lblimagen);
		
		btn_aņadirUsuario = new JButton("new user");
		btn_aņadirUsuario.addMouseListener(new Btn_aņadirUsuarioMouseListener());
		btn_aņadirUsuario.setBounds(335, 494, 89, 23);
		contentPane.add(btn_aņadirUsuario);

		passwordField.addFocusListener(new PasswordFieldFocusListener());
		btn_ENTRAR.addMouseListener(new Btn_ENTRARMouseListener());
		txt_Usuario.addFocusListener(new Txt_UsuarioFocusListener());
	}

	private class Btn_ENTRARMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			boolean corector=false;
			String us = txt_Usuario.getText();
			char[] pass = passwordField.getPassword();
			String passComprobar = "";
			for (int i = 0; i < pass.length; i++) {
				passComprobar += pass[i];
			}

			
			for (Iterator iterator = users.iterator(); iterator.hasNext();) {
				Usuario usr = (Usuario) iterator.next();
				if (usr.getNombre().equals(us) && usr.getPassWord().equals(passComprobar)) {
					JOptionPane.showMessageDialog(null, "acceso permitido", "acceso permitido", 1);
					dispose();//eliminar ventana
					Interfaz i=new Interfaz(usr.getNombre());
					i.setVisible(true);
					corector=true;
					break;
				}else {
					corector=false;
				}
				
				
			}
			
//			if (us.equals(usuario) && passComprobar.equals(contraseņa)) {
//				JOptionPane.showMessageDialog(null, "acceso permitido", "acceso permitido", 1);
//				corector=true;
//				dispose();//eliminar ventana
//				Interfaz i=new Interfaz(us);
//				i.setVisible(true);
//				//dispose();
//				//setVisible(false);
//			} else {
//				corector=false;
//				//JOptionPane.showMessageDialog(null, "acceso denegado", "acceso denegado", 0);
//				Desktop enlace = Desktop.getDesktop();
//				try {
//					enlace.browse(new URI("https://www.youtube.com/watch?v=dQw4w9WgXcQ"));
//				} catch (IOException | URISyntaxException e3) {
//				}
//			}
			if (!corector) {
				JOptionPane.showMessageDialog(null, "acceso denegado", "acceso denegado", 0);
			}
		}
	}

	private class Txt_UsuarioFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent arg0) {
			if (txt_Usuario.getText().equals("usuario")) {
				txt_Usuario.setText("");
				txt_Usuario.setForeground(Color.BLACK);
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			if (txt_Usuario.getText().isEmpty()) {
				txt_Usuario.setForeground(Color.LIGHT_GRAY);
				txt_Usuario.setFont(new Font("Tahoma", Font.ITALIC, 11));
				txt_Usuario.setText("usuario");
			}
		}
	}

	private class PasswordFieldFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent arg0) {
			String pass="";
			char ch[]=passwordField.getPassword();
			for (int i = 0; i < ch.length; i++) {
				pass+=ch[i];
			}
			
			if (pass.equals("contraseņa")) {
				passwordField.setText("");
				passwordField.setEchoChar('*');
				passwordField.setForeground(Color.BLACK);
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			String x = "contraseņa";
			String pass="";
			char ch[]=passwordField.getPassword();
			for (int i = 0; i < ch.length; i++) {
				pass+=ch[i];
			}
			
			if (pass.equals("")) {
				passwordField.setForeground(Color.LIGHT_GRAY);
				passwordField.setEchoChar((char) 0);
				passwordField.setText(x);
			}
		}
	}
	private class Txt_UsuarioKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent arg0) {
			comprobarBTN();
		}
	}
	private class PasswordFieldKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			comprobarBTN();
		}
	}
	private class Btn_aņadirUsuarioMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			
			
			InterfazUsuarios iu=new InterfazUsuarios();
			iu.setVisible(true);
			dispose();
			
			
			
			
		}
	}
	private void comprobarBTN() {
		if (txt_Usuario.getText().equals("usuario")||txt_Usuario.getText().equals("usuario") || passwordField.getText().equals("") || passwordField.getText().equals("contraseņa")) {
			btn_ENTRAR.setEnabled(false);
		}else {
			btn_ENTRAR.setEnabled(true);
		}

	}
}
