package agendaDinamica;

import java.io.Serializable;

public class Contacto implements Serializable {
	private int telefono;
	private String nombre;
	private boolean fijo;

	public Contacto(int telefono, String nombre, boolean fijo) {
		this.telefono = telefono;
		this.nombre = nombre;
		this.fijo = fijo;// true= masculino
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		String fijo = "Contacto [telefono=" + telefono + ", nombre=" + nombre + " fijo]";
		String movil = "Contacto [telefono=" + telefono + ", nombre=" + nombre + " movil]";
		if (this.fijo) {
			return fijo;
		} else {
			return movil;
		}

	}

}
