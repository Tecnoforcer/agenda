package agendaDinamica;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import login.InterfazLogin;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JProgressBar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Color;
import java.awt.*;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Interfaz extends JFrame {

	private int contUSELESSBTTN;
	private JPanel contentPane;
	private JLabel lblTitulo;
	private JLabel lblNOMBRE;
	private JLabel lblTELEFONO;
	private JButton btnGUARDAR;
	private JTextArea TA_GUARDADOS;
	private DefaultComboBoxModel<String> modeloCB;
	private JProgressBar pb_MEM_USED;
	private ArrayList<Contacto> contacto;
	private JLabel lbl_InfoContacto;
	private JTextField textField_Nombre;
	private JTextField textField_edad;
	private JLabel lblTipo;
	private JRadioButton rdbtn_fijo;
	private JRadioButton rdbtn_movil;
	private boolean fijo;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btndelete;
	private JButton btn_EXIT;
	private JButton btn_USELESS;
	private String user;
	private DefaultListModel modLis;
	private JList list;
	private JTextField textField_Buscar;
	private JLabel lbl_buscar;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Interfaz frame = new Interfaz();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public Interfaz(String usr) {
		user = usr;
		contacto = IOdatos.cargarDatos(user);
		fijo = false;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 911, 737);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 255, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblTitulo = new JLabel("         AGENDA");
		lblTitulo.setFont(new Font("Comic Sans MS", Font.BOLD, 32));
		lblTitulo.setBounds(239, 28, 364, 36);
		contentPane.add(lblTitulo);

		lblNOMBRE = new JLabel("NOMBRE");
		lblNOMBRE.setBounds(68, 118, 92, 22);
		contentPane.add(lblNOMBRE);

		lblTELEFONO = new JLabel("TELEFONO");
		lblTELEFONO.setBounds(68, 151, 92, 22);
		contentPane.add(lblTELEFONO);

		textField_Nombre = new JTextField();
		textField_Nombre.setBounds(170, 119, 160, 20);
		contentPane.add(textField_Nombre);
		textField_Nombre.setColumns(10);

		textField_edad = new JTextField();
		textField_edad.setBounds(170, 152, 160, 20);
		contentPane.add(textField_edad);
		textField_edad.setColumns(10);

		btnGUARDAR = new JButton("GUARDAR");

		btnGUARDAR.setBounds(68, 208, 262, 36);
		contentPane.add(btnGUARDAR);
		modeloCB = new DefaultComboBoxModel();

		pb_MEM_USED = new JProgressBar();
		pb_MEM_USED.setStringPainted(true);
		pb_MEM_USED.setForeground(new Color(204, 0, 0));
		pb_MEM_USED.setMaximum(90);
		pb_MEM_USED.setBounds(68, 243, 262, 14);
		contentPane.add(pb_MEM_USED);

		lbl_InfoContacto = new JLabel("");
		lbl_InfoContacto.setBounds(463, 392, 422, 47);
		contentPane.add(lbl_InfoContacto);

		lblTipo = new JLabel("tipo");
		lblTipo.setBounds(68, 184, 92, 14);
		contentPane.add(lblTipo);

		rdbtn_fijo = new JRadioButton("fijo");
		buttonGroup.add(rdbtn_fijo);
		rdbtn_fijo.setBounds(170, 179, 92, 23);
		contentPane.add(rdbtn_fijo);

		rdbtn_movil = new JRadioButton("movil");
		buttonGroup.add(rdbtn_movil);
		rdbtn_movil.setBounds(266, 179, 89, 23);
		contentPane.add(rdbtn_movil);

		TA_GUARDADOS = new JTextArea();
		TA_GUARDADOS.setEditable(false);
		TA_GUARDADOS.setBounds(463, 118, 422, 139);
		contentPane.add(TA_GUARDADOS);

		btndelete = new JButton("del");

		btndelete.setBounds(455, 459, 430, 36);
		contentPane.add(btndelete);

		btn_EXIT = new JButton("salir");
		btn_EXIT.addMouseListener(new Btn_EXITMouseListener());
		btn_EXIT.setBounds(68, 640, 722, 47);
		contentPane.add(btn_EXIT);

		btn_USELESS = new JButton("click 20 times for surprise");
		btn_USELESS.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 10));
		btn_USELESS.addMouseListener(new Btn_USELESSMouseListener());
		btn_USELESS.setBounds(463, 506, 333, 36);
		contentPane.add(btn_USELESS);

		list = new JList();
		list.addMouseListener(new ListMouseListener());
		modLis = new DefaultListModel();

		list.setModel(modLis);
		list.setBounds(68, 300, 300, 220);
		contentPane.add(list);

		textField_Buscar = new JTextField();
		textField_Buscar.addKeyListener(new TextField_BuscarKeyListener());
		// textField_Buscar.addActionListener(new TextFieldActionListener());
		textField_Buscar.setBounds(68, 531, 300, 20);
		contentPane.add(textField_Buscar);
		textField_Buscar.setColumns(10);

		lbl_buscar = new JLabel("Buscar");
		lbl_buscar.setBounds(202, 556, 46, 14);
		contentPane.add(lbl_buscar);
		btnGUARDAR.addMouseListener(new BtnGUARDARMouseListener());
		btndelete.addMouseListener(new BtndeleteMouseListener());

		modeloCB.removeAllElements();
		modeloCB.addElement("Contactos");

		actualizar();
	}

	private class BtnGUARDARMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			String nombre = "";
			String tel = "";
			int telefono = 0;
			boolean correct = false;

			do {
				nombre = textField_Nombre.getText();
				if (!nombre.equalsIgnoreCase("contactos")) {
					correct = true;
				}

			} while (!correct);

			tel = (textField_edad.getText());
			if (tel != "" && tel != null) {
				try {
					telefono = Integer.parseInt(tel);
					// System.out.println(tel);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(rootPane, "introduce los datos correctamente", "error", 0);
				}

				if (rdbtn_fijo.isSelected()) {
					fijo = true;
				} else if (rdbtn_movil.isSelected()) {
					fijo = false;
				} else {
					fijo = false;
				}

				Contacto cont = new Contacto(telefono, nombre, fijo);
//				for (int i = 0; i < contacto.size(); i++) {
//					if (contacto.get(i) == null) {
				contacto.add(cont);
				textField_Nombre.setText("");
				textField_edad.setText("");
				JOptionPane.showMessageDialog(rootPane, "Contacto Guardado", "guardado", 3);
//						break;
//					}
//				}

			} else {
				JOptionPane.showMessageDialog(rootPane, "introduce los datos", "error", 2);
			}

			actualizar();
			IOdatos.guardar_Datos(contacto, user);
		}
	}

	private void actualizar() {
		modLis.removeAllElements();
		// modeloCB.addElement("Contactos");

		String salida = "";
		int contador = 0;
//		for (int i = 0; i < contacto.size(); i++) {
//			if (contacto.get(i) != null) {
//				salida += contacto.get(i).getNombre() + "\n";
//				contador++;
//				modeloCB.addElement(contacto.get(i).getNombre());
//			}
//		}

//		for (Contacto cont : contacto) {
//			if (cont != null) {
//				salida += cont.getNombre() + "\n";
//				contador++;
//				modeloCB.addElement(cont.getNombre());
//			}
//		}

		for (Iterator iterator = contacto.iterator(); iterator.hasNext();) {
			Contacto cont = (Contacto) iterator.next();
			if (cont != null) {
				salida += cont.getNombre() + "\n";
				contador++;
				modLis.addElement(cont.getNombre());
			}
		}

		pb_MEM_USED.setValue(contador);
		TA_GUARDADOS.setText(salida);

	}

	private class BtndeleteMouseListener extends MouseAdapter {
		@Override
		public void mouseReleased(MouseEvent e) {

			int buscar = 0;

			buscar = list.getSelectedIndex();
//			for (int i = 0; i < contacto.size(); i++) {
//				if (contacto.get(i) != null && contacto.get(i).getNombre().equals(buscar)) {
			contacto.remove(buscar);
//					break;
//				}
//			}

			IOdatos.guardar_Datos(contacto, user);
			lbl_InfoContacto.setText("");
			actualizar();
		}
	}

	private class Btn_EXITMouseListener extends MouseAdapter {
		@Override
		public void mouseReleased(MouseEvent arg0) {
			InterfazLogin il = new InterfazLogin();
			il.setVisible(true);
			dispose();
		}
	}

	private class Btn_USELESSMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			contUSELESSBTTN++;
			if (contUSELESSBTTN == 20) {
				// JOptionPane.showMessageDialog(rootPane,
				// "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "random window", 2);
				Desktop enlace = Desktop.getDesktop();
				try {
					enlace.browse(new URI("https://www.youtube.com/watch?v=dQw4w9WgXcQ"));
				} catch (IOException | URISyntaxException e3) {

				}
				contUSELESSBTTN = 0;
			}

		}
	}

	private class TextField_BuscarKeyListener extends KeyAdapter {


		@Override
		public void keyPressed(KeyEvent e) {
			String buscado ="";
			buscado = textField_Buscar.getText();
			buscado+="[a-zA-Z_0-9]*";
			modLis.removeAllElements();
			for (Contacto c : contacto) {
				if (Pattern.matches(buscado, c.getNombre())) {
					modLis.addElement(c.getNombre());
				}
			}
		}
		@Override
		public void keyReleased(KeyEvent e) {
			String buscado ="";
			buscado = textField_Buscar.getText();
			buscado+="[a-zA-Z_0-9]*";
			modLis.removeAllElements();
			for (Contacto c : contacto) {
				if (Pattern.matches(buscado, c.getNombre())) {
					modLis.addElement(c.getNombre());
				}
			}
		}
	}
	private class ListMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			String buscado=(String) list.getModel().getElementAt(list.getSelectedIndex());
			for (Contacto c : contacto) {
				if (c.getNombre().equals(buscado)) {
					lbl_InfoContacto.setText(c.toString());
				}
			}
			
		}
	}
}
