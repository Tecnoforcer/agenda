package agendaDinamica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class IOdatos {
	private final static String directorio = "datosUsuario/U_";
	//private final static String fichero = directorio+"contactos.dat";

	public static void guardar_Datos(ArrayList<Contacto> vContactos, String strUser) {
		File dir = new File(directorio+strUser);
		File file = null;
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		if (!dir.exists()) {
			dir.mkdir();
		}
		System.out.println(dir.getAbsolutePath());
		file = new File(dir.getAbsoluteFile()+"/contactos.dat");
	

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// e.printStackTrace();
			}
		} else {
			try {
				fos = new FileOutputStream(file);
				oos = new ObjectOutputStream(fos);

				oos.writeObject(vContactos);

			} catch (FileNotFoundException e) {
				// e.printStackTrace();
			} catch (IOException e) {
				// e.printStackTrace();
			} finally {
				try {
					oos.close();
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static ArrayList<Contacto> cargarDatos(String strUser) {
		ArrayList<Contacto> vContact = new ArrayList<Contacto>();
		File dir = new File(directorio+strUser);
		File file = null;
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		if (!dir.exists()) {
			dir.mkdir();
		}
		
		System.out.println(dir.getAbsolutePath());
		file = new File(dir.getAbsoluteFile()+"/contactos.dat");

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return vContact;
		}
		try {
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);
//				ois.readObject();
			vContact = (ArrayList<Contacto>) ois.readObject();

		} catch (FileNotFoundException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// e.printStackTrace();
		} finally {
			try {
				if (ois!=null)
					ois.close();
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		}

		return vContact;

	}

}
